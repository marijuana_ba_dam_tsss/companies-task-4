<?	require_once 'header.php';
	global $template;
	$data = $template->get ('card');
	$model = new Companies ();
?>
	<div id="content">
		<form method="post" action="#" id="myForm">
			<table>
				<?foreach ($data as $key => $value):?>
					<tr>
						<td>
							<?=$model->fields_for_viewing[$key]['name']?>
						</td>
						<td>
							<?=$value?>
						</td>
					</tr>
				<?endforeach?>
			</table>
			<p><input type="submit" name="sendBtn" value="Отправить" /></p>
		</form>
	</div>
	<script type="text/javascript" src="/js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="/js/jquery.json-1.3.js"></script>
    <script type="text/javascript">
        $(function() 
		{	//отправить данные (json) на сервер
            $("#myForm").submit(function() 
			{        
				var formData = 
				{	//это ужасно, вообще не круто
					'name' : '<?=$data['name']?>', 
					'annotation' : '<?=$data['annotation']?>',
					'info' : '<?=$data['info']?>',
					'time_of_work' : '<?=$data['time_of_work']?>',
					'formated_address' : '<?=$data['formated_address']?>'
                };
                $.ajax(
				{
                    url:'/companies/parserJson', 
					type:'POST', 
					data:'jsonData=' + $.toJSON(formData), 
					success: function(res) 
					{
                        alert(res);
                    }
                });
                return false;
            });
        });
    </script>
<?	require_once 'footer.php';?>
