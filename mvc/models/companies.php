<?	class Companies
	{
		public
			$fields  =
			[
				'id',
				'name', 
				'owner', 
				'info', 
				'page_name', 
				'additional_contacts', 
				'adress', 
				'email', 
				'comp_url', 
				'logo', 
				'main_image', 
				'is_auth', 
				'english_name', 
				'is_rus_name', 
				'manager_id', 
				'date_created', 
				'date_owner', 
				'fiz_city', 
				'fiz_adress', 
				'urid_city', 
				'urid_adress', 
				'key_words', 
				'vote_for', 
				'vote_against', 
				'glossary_name', 
				'points', 
				'in_catalog', 
				'on_dengi_id', 
				'additional_cat', 
				'search_string', 
				'baza_service', 
				'baza_portfolio',
				'baza_materials',
				'is_paid',
				'colored',
				'annotation',
				'seo_title',
				'seo_keywords',
				'seo_description',
				'time_of_work',
				'date_updated',
				'admin_note',
				'owner_visit_date',
				'latitude',
				'longitude',
				'additional_attributes',
				'is_hidden',
				'add_attr',
				'is_location_moderated',
				'formated_address',
				'last_change',
				'is_address_located',
				'admin_annotation',
				'baza_user_id',
				'sent_to_robot',
				'realm',
				'deleted'
			],
			
			$fields_for_viewing =
			[
				'name' => ['name' => 'Название'], 
				'annotation' => ['name' => 'Аннотация'],
				'info' => ['name' => 'Информация'],
				'time_of_work' => ['name' => 'Время работы'],
				'formated_address' => ['name' => 'Адрес'],
			];
			
			function getCard ($id)
			{
				global $db;
				$query = "SELECT ".implode (', ', array_keys ($this->fields_for_viewing))." FROM companies WHERE id = :id";	
				$result = $db->prepare ($query);
				$result->execute (["id" => $id]);
				$data = $result->fetchAll (PDO::FETCH_ASSOC);
				return $data[0];
			}
	}
?>